/* fuzz - print many unicode chars to the terminal.
 * 
 * This program will print every 4-byte combination to the
 * terminal (though, of course, this could take a *long* time...)
 * 
 * Copyright (C) 2017 Ethan McTague
 * Email <ethan@tague.me> for more information.
 * Licensed under the BSD license - see LICENSE.
 */
#include <stdio.h>
#include <string.h>

void fuzz(const char* before, int num);
void help(const char* progName);
void version();
void badArgs(const char* progName);
void readArgs(int argc, char* argv[], int* showHelp, int* showVersion, int* runFuzz);

int main (int argc, char* argv[]) {
  int showHelp = 0;
  int showVersion = 0;
  int runFuzz = 0;
  readArgs(argc, argv, &showHelp, &showVersion, &runFuzz);

  if (showHelp) help(argv[0]);
  else if (showVersion) version();
  else if (runFuzz) fuzz("", 4);
  else badArgs(argv[0]);
  return 0;
}

void fuzz(const char* before, int num) {
  for (int ch = 0; ch < 0x7FFFFFF; ch++) {
    int size = num + strlen(before) + 1;
    char buffer[size];
    snprintf(buffer, size, "%s%c", before, ch);
    if (num > 0) fuzz(buffer, num - 1);
    else printf("%s", buffer);
  }
}

void help(const char* progName) {
  puts("fuzzer: print many unicode chars to the terminal.");
  puts("Pipe output to another program to test its input handling.");
  puts("");
  printf("Usage: %s [options]\n", progName);
  puts("Options:");
  puts(" --help     Print this help screen.");
  puts(" --version  Print version information.");
  puts(" --fuzz     Run the Fuzzer.");
  puts("");
  puts("Copyright (C) 2017 Ethan McTague");
  puts("Email <ethan@tague.me> for more information.");
}

void version() {
  puts("fuzzer 0.0.1");
  puts("By Ethan McTague");
}

void badArgs(const char* progName) {
  printf("Bad arguments. Run '%s --help' to learn how to use fuzzer.\n", progName);
}

void readArgs(int argc, char* argv[], int* showHelp, int* showVersion, int* runFuzz) {
  for (int i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "--help")) {
      *showHelp = 1;
      return;
    } else if (!strcmp(argv[i], "--version")) {
      *showVersion = 1;
      return;
    } else if (!strcmp(argv[i], "--fuzz")) {
      *runFuzz = 1;
      return;
    }
  }
}
