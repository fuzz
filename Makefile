CC=gcc
CFLAGS=-Wall
LFLAGS=
BIN=fuzz
OBJECTS=fuzz.o
PREFIX=/usr/local

$(BIN): $(OBJECTS)
	$(CC) $(OBJECTS) $(LFLAGS) -o $(BIN)

%.o: %.c
	$(CC) -c $< $(CFLAGS) -o $@

fuzz.1.bz2: fuzz.1
	bzip2 -c fuzz.1 > fuzz.1.bz2

install: $(BIN) fuzz.1.bz2
	install -m 0755 $(BIN) $(PREFIX)/bin
	install -m 0644 fuzz.1.bz2 $(PREFIX)/man/man1

.PHONY: install

clean:
	rm -rf $(OBJECTS) $(BIN) fuzz.1.bz2

